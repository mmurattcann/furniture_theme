@extends("layouts.frontend._layout")



@push("css")

@endpush


@section("content")
    <!--====================  hero slider area ====================-->

    <div class="hero-slider-area section-space">
        <!-- START REVOLUTION SLIDER 5.4.7 fullwidth mode -->
        <div id="rev_slider_30_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7">
            <ul>
                <!-- SLIDE  -->
                @foreach($sliders as $index => $slider)
                    <li data-index="rs-7{{ $index }}" data-transition="fade,parallaxtoright,parallaxtoleft,parallaxtotop,parallaxtobottom,parallaxhorizontal,parallaxvertical" data-slotamount="default,default,default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default,default,default,default,default,default,default" data-easeout="default,default,default,default,default,default,default" data-masterspeed="1000,default,default,default,default,default,default" data-thumb="" data-delay="7000" data-rotate="0,0,0,0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ frontImage("sliders", $slider->image) }}" alt="{{$slider->title}}" width="1920" height="1080" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption 20   tp-resizeme" id="slide-72-layer-18" data-x="['center','left','center','center']" data-hoffset="['5','269','0','6']" data-y="['middle','middle','middle','top']" data-voffset="['0','0','0','352']" data-fontsize="['20','20','20','18']" data-lineheight="['32','32','32','20']" data-fontweight="['700','600','600','700']" data-color="['rgba(0,0,0,0.4)','rgba(17,17,17,0.4)','rgba(17,17,17,0.4)','rgba(17,17,17,0.4)']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":730,"speed":1510,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power2.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 20px; line-height: 32px; font-weight: 700; color: rgba(0,0,0,0.4); letter-spacing: 0px;font-family:Open Sans;">
                            {{$slider->description}}</div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption   tp-resizeme" id="slide-72-layer-20" data-x="['center','left','center','left']" data-hoffset="['2','135','2','36']" data-y="['top','top','top','top']" data-voffset="['357','238','357','272']" data-fontsize="['80','80','70','45']" data-lineheight="['96','96','80','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":720,"speed":1540,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 80px; line-height: 96px; font-weight: 600; color: #000000; letter-spacing: 0px;font-family:Open Sans;">{{ $slider->title }}</div>

                        @if($slider->has_button == 1)
                            <!-- LAYER NR. 3 -->
                            <a class="tp-caption Robin-Button-New-2 rev-btn " href="{{$slider->url}}" target="_self" id="slide-72-layer-9" data-x="['center','center','center','left']" data-hoffset="['0','0','0','182']" data-y="['top','top','top','top']" data-voffset="['572','449','541','411']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-actions='' data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":990,"speed":1030,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(249,178,0);bc:rgb(249,178,0);"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[25,25,25,25]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[25,25,25,25]" style="z-index: 7; white-space: nowrap; font-size: 16px; line-height: 21px; font-weight: 700; color: #000000; font-family:Source Sans Pro;background-color:rgba(247,177,19,0);border-color:rgba(0,0,0,1);border-style:solid;border-width:2px 2px 2px 2px;border-radius:5px 5px 5px 5px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;"> {{ $slider->button_title }}</a>
                        @endif
                    </li>
                @endforeach

            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>

    <!--=====
   ====================  banner scale section ====================-->

    <div class="banner-area section-space">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  banner scale wrapper  =======-->

                    <div class="banner-scale-wrapper">
                        <div class="row">
                            @foreach($boxes as $box)

                                <div class="col-md-6">
                                    <!--=======  single banner scale  =======-->

                                    <div class="single-banner single-banner--scale">
                                        <div class="single-banner--scale__image">
                                            <img src="{{ $box["image"] }}" class="img-fluid" alt="{{$box["title"] }}">
                                        </div>
                                        <div class="single-banner--scale__content">
                                            <p class="title" >{{ $box["title"] }}</p>
                                            <p class="subtitle" >{!! $box["description"] !!}</p>
                                            <a href="{{ $box["route"] }}" class="theme-button theme-button--banner--scale">{{ $box["buttonText"] }}</a>
                                        </div>
                                    </div>

                                    <!--=======  End of single banner scale  =======-->
                                </div>

                            @endforeach
                        </div>
                    </div>

                    <!--=======  End of banner scale wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of banner scale section  ====================-->
    <!--====================  product slider with text area ====================-->

    <div class="product-slider-text-area section-space">
        <!--=======  product slider with text wrapper  =======-->

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product-slider-text-wrapper">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="product-slider-text-wrapper__text">
                                    <h2 class="title">Öne Çıkanlar</h2>
                                    <p class="description">Sizin için seçtiğimiz ürünleri inceleyerek hemen alışverişe başlayabilir ve hayatınıza renk katabilirsiniz.</p>
                                    <a href="shop-left-sidebar.html" class="slider-text-link">Tüm Ürünler <i class="fa fa-caret-right"></i></a>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <!--=======  product slider wrapper  =======-->

                                <div class="product-slider-wrapper theme-slick-slider" data-slick-setting='{
                                        "slidesToShow": 3,
                                        "slidesToScroll": 3,
                                        "arrows": true,
                                        "dots": false,
                                        "autoplay": false,
                                        "speed": 500,
                                        "prevArrow": {"buttonClass": "slick-prev", "iconClass": "fa fa-angle-left" },
                                        "nextArrow": {"buttonClass": "slick-next", "iconClass": "fa fa-angle-right" }
                                    }' data-slick-responsive='[
                                        {"breakpoint":1501, "settings": {"slidesToShow": 3, "slidesToScroll": 3, "arrows": false} },
                                        {"breakpoint":1199, "settings": {"slidesToShow": 2, "slidesToScroll": 2, "arrows": false} },
                                        {"breakpoint":991, "settings": {"slidesToShow": 2,"slidesToScroll": 2, "arrows": true, "dots": false} },
                                        {"breakpoint":767, "settings": {"slidesToShow": 2,"slidesToScroll": 2,  "arrows": true, "dots": false} },
                                        {"breakpoint":575, "settings": {"slidesToShow": 2, "slidesToScroll": 2,"arrows": true, "dots": false} },
                                        {"breakpoint":479, "settings": {"slidesToShow": 1,"slidesToScroll": 1, "arrows": false, "dots": false} }
                                    ]'>

                                    @foreach($featuredProducts as $fproduct)
                                        <div class="col">
                                        <!--=======  single short view product  =======-->

                                        <div class="single-grid-product">
                                            <div class="single-grid-product__image">
                                                <div class="product-badge-wrapper">
                                                    <span class="hot">Öne Çıkarılan</span>
                                                </div>
                                                <a href="{{ $fproduct["route"] }}" class="image-wrap">
                                                    <img src="{{ $fproduct["coverImage"] }}" class="img-fluid" alt="{{$fproduct["title"]}}">
                                                    <img src="{{ $fproduct["secondCover"] }}" class="img-fluid" alt="{{$fproduct["title"]}}">
                                                </a>
                                                <div class="product-hover-icon-wrapper">
                                                    <span class="single-icon single-icon--add-to-cart"><a href="#" data-tippy="Sepete Ekle" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme = "sharpborder" ><i class="fa fa-shopping-basket"></i> <span>Sepete Ekle</span></a></span>
                                                    <!--<span class="single-icon single-icon--compare"><a href="#" data-tippy="Compare" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme = "sharpborder" ><i class="fa fa-exchange"></i></a></span>-->
                                                </div>
                                            </div>
                                            <div class="single-grid-product__content">
                                                <h3 class="title"><a href="{{ $fproduct["route"] }}">{{ $fproduct["title"] }}</a></h3>
                                                <div class="price"><span class="main-price {{ $fproduct["discountPrice"] != 0 ? "discounted": "" }}">{{ $fproduct["price"] }}</span>
                                                    @if($fproduct["discountPrice"] != 0)
                                                        <span class="discounted-price">{{ $fproduct["discountPrice"] }}</span>
                                                    @endif
                                                </div>
                                                <div class="rating">
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>

                                                <a href="#" class="favorite-icon" data-tippy="Add to Wishlist" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder" data-tippy-placement="left">
                                                    <i class="fa fa-heart-o"></i>
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                            </div>
                                        </div>

                                        <!--=======  End of single short view product  =======-->
                                    </div>
                                    @endforeach
                                </div>
                                <!--=======  End of product slider wrapper  =======-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--=======  End of product slider with text wrapper  =======-->
    </div>

    <!--====================  End of product slider with text area  ====================-->
    <!--====================  product double row area ====================-->

    <div class="product-double-row-area section-space">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="section-title-area text-center">
                        <h2 class="section-title">Başlıca Ürünler</h2>
                        <p class="section-subtitle">Avlu Home & Garden'ın kaliteli ve eşsiz ürünlerinin tadını çıkarın</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!--=======  product row wrapper  =======-->

                    <div class="product-row-wrapper">
                        <div class="row">
                            @foreach($products as $product)
                                <div class="col-lg-3 col-md-4 col-sm-6 col-custom-sm-6">
                                <!--=======  single short view product  =======-->

                                <div class="single-grid-product">
                                    <div class="single-grid-product__image">
                                        <div class="product-badge-wrapper">
                                            <span class="hot">Yeni</span>
                                        </div>
                                        <a href="{{ $product["route"] }}" class="image-wrap">
                                            <img src="{{ $product["coverImage"] }}" class="img-fluid" alt="{{ $product["title"] }}">
                                            <img src="{{ $product["secondCover"] }}" class="img-fluid" alt="{{ $product["title"] }}">
                                        </a>
                                        <div class="product-hover-icon-wrapper">

                                            <span class="single-icon single-icon--add-to-cart"><a class="addCart" href="javascript:void(0);" data-id="{{ $product["id"] }}" data-route="{{ route("front.addToChart") }}" data-tippy="Sepete Ekle" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme = "sharpborder" ><i class="fa fa-shopping-basket"></i> <span>Sepete Ekle</span></a></span>
                                        </div>
                                    </div>
                                    <div class="single-grid-product__content">
                                        <h3 class="title"><a href="{{ $product["route"] }}">{{ $product["title"] }}</a></h3>
                                        <div class="price"><span class="main-price {{ $product["discountPrice"] != 0 ? "discounted": "" }}">{{ $product["price"] }}</span>
                                            @if($product["discountPrice"] != 0)
                                                <span class="discounted-price">{{ $product["discountPrice"] }}</span>
                                            @endif
                                        </div>
                                       <div class="rating">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star"></i>
                                        </div>

                                        <a href="#" class="favorite-icon" data-tippy="Add to Wishlist" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder" data-tippy-placement="left">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>
                                    </div>
                                </div>

                                <!--=======  End of single short view product  =======-->
                            </div>
                            @endforeach
                        </div>
                    </div>

                    <!--=======  End of product row wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of product double row area  ====================-->

    <!--====================  blog slider ====================-->

    <div class="blog-slider-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <div class="section-title-area text-center">
                        <h2 class="section-title">Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!--=======  blog slider wrapper  =======-->

                    <div class="blog-slider-wrapper theme-slick-slider" data-slick-setting='{
                        "slidesToShow": 3,
                        "slidesToScroll": 3,
                        "arrows": true,
                        "autoplay": false,
                        "autoplaySpeed": 5000,
                        "speed": 500,
                        "prevArrow": {"buttonClass": "slick-prev", "iconClass": "fa fa-angle-left" },
                        "nextArrow": {"buttonClass": "slick-next", "iconClass": "fa fa-angle-right" }
                    }' data-slick-responsive='[
                        {"breakpoint":1501, "settings": {"slidesToShow": 3, "arrows": false} },
                        {"breakpoint":1199, "settings": {"slidesToShow": 3, "arrows": false} },
                        {"breakpoint":991, "settings": {"slidesToShow": 2, "arrows": false, "slidesToScroll": 2} },
                        {"breakpoint":767, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                        {"breakpoint":575, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                        {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} }
                    ]'>

                        @foreach($blogs as $blog)
                            <!--=======  single blog post  =======-->
                            <div class="col">

                                <div class="single-slider-blog-post">
                                    <div class="single-slider-blog-post__image">
                                        <a href="{{ $blog["route"] }}">
                                            <img src="{{ $blog["image"] }}" class="img-fluid" alt="{{ $blog["title"] }}">
                                        </a>
                                    </div>
                                    <div class="single-slider-blog-post__content">
                                        <h3 class="post-title"><a href="{{ $blog["route"] }}">{{ $blog["title"] }}</a></h3>
                                        <p class="post-meta"></span> <a href="#">{{ $blog["created_at"]}}</a></p>
                                        <p class="post-excerpt">{!! $blog["short_description"] !!}</p>
                                        <a href="{{ $blog["route"] }}" class="blog-readmore-link">Devamını Oku <i class="fa fa-caret-right"></i></a>
                                    </div>
                                </div>

                            </div>
                            <!--=======  End of single blog post  =======-->
                        @endforeach

                    </div>

                    <!--=======  End of blog slider wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of blog slider  ====================-->

@endsection

@push("js")

@endpush
