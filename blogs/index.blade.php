@extends("layouts.frontend._layout")

@section("title")
{{ $title  }}
@endsection

@push("css")

@endpush

@section("content")
    <div class="breadcrumb-area section-space--breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <!--=======  breadcrumb wrapper  =======-->

                    <div class="breadcrumb-wrapper">
                        <h2 class="page-title">Blog</h2>
                        <ul class="breadcrumb-list">
                            <li><a href="{{ route("front.index") }}">Anasayfa</a></li>
                            <li class="active">Blog</li>
                        </ul>
                    </div>

                    <!--=======  End of breadcrumb wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of breadcrumb area  ====================-->

    <!--====================  page content wrapper ====================-->

    <div class="page-content-wrapper">
        <!--=======  blog page area  =======-->

        <div class="blog-page-area">
            <div class="container">
                <div class="row">

                    <div class="col-lg-8 offset-md-2">
                        <!--=======  blog post wrapper  =======-->

                        <div class="blog-post-wrapper">
                            <div class="row">
                                @foreach($blogs as $blog)
                                    <div class="col-md-6">
                                        <!--=======  single post  =======-->

                                        <div class="single-blog-post">
                                            <div class="single-blog-post__image">
                                                <a href="{{ $blog["route"] }}">
                                                    <img src="{{ $blog["image"] }}" class="img-fluid" alt="{{ $blog["title"] }}">
                                                </a>
                                            </div>
                                            <div class="single-blog-post__content">
                                                <h3 class="post-title"><a href="{{ $blog["route"] }}">{{ $blog["title"] }}</a></h3>
                                                <p class="post-meta"> {{ $blog["created_at"] }}</p>
                                                <p class="post-excerpt">{{ $blog["short_description"] }}</p>
                                                <a href="{{ $blog["route"] }}" class="blog-readmore-link">Devamını Oku <i class="fa fa-caret-right"></i></a>
                                            </div>
                                        </div>

                                        <!--=======  End of single post  =======-->
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <!--=======  End of blog post wrapper  =======-->
                        <!--=======  pagination wrapper  =======-->
    <!--
                        <div class="pagination-wrapper">
                            <ul>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
-->
                        <!--=======  End of pagination wrapper  =======-->
                    </div>
                </div>
            </div>
        </div>

        <!--=======  End of blog page area  =======-->

    </div>


@endsection

@push("js")

@endpush
