@extends("layouts.frontend._layout")

@section("title")
    {{ $title  }}
@endsection

@push("css")

@endpush

@section("content")
    <div class="breadcrumb-area section-space--breadcrumb ">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <!--=======  breadcrumb wrapper  =======-->

                    <div class="breadcrumb-wrapper">
                        <h2 class="page-title">{{$blog->title}}</h2>
                        <ul class="breadcrumb-list">
                            <li><a href="{{ route("front.index") }}">Anasayfa</a></li>
                            <li><a href="{{ route("front.blog-list") }}">Blog</a></li>
                            <li class="active">{{ $blog->title }}</li>
                        </ul>
                    </div>

                    <!--=======  End of breadcrumb wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of breadcrumb area  ====================-->

    <!--====================  page content wrapper ====================-->

    <div class="page-content-wrapper">
        <!--=======  blog page area  =======-->

        <div class="blog-page-area">
            <div class="container">
                <div class="row">

                    <div class="col-lg-8 offset-md-2 order-1">
                        <!--=======  blog single post details wrapper  =======-->

                        <div class="blog-single-post-details-wrapper">

                            <h2 class="post-title">{{ $blog->title }}</h2>
                            <p class="post-meta">{{ turkishDate($blog->created_at) }}</p>

                            <div class="post-thumbnail">
                                <img src="{{ getImage("blogs", $blog->image) }}" class="img-fluid" alt="">
                            </div>

                            <div class="post-text-content">

                                <p>
                                    {!! $blog->content !!}
                                </p>

                            </div>

                        </div>

                        <!--=======  End of blog single post details wrapper  =======-->
                        <!--=======  blog related post  =======-->

                        <div class="blog-related-post-area">

                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 class="blog-details-section-title">İlginizi Çekebilir</h3>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <!--=======  blog related post wrapper  =======-->

                                    <div class="blog-related-post-wrapper">
                                        <div class="row">
                                            @foreach($similars as $similar)
                                                <div class="col-md-4">
                                                    <!--=======  single related post  =======-->

                                                    <div class="single-blog-post single-blog-post--related">
                                                        <div class="single-blog-post__image">
                                                            <a href="{{ $similar["route"] }}">
                                                                <img src="{{ $similar["image"] }}" class="img-fluid" alt="{{ $similar["title"] }}">
                                                            </a>
                                                        </div>
                                                        <div class="single-blog-post__content">
                                                            <h3 class="post-title"><a href="{{ $similar["route"] }}">{{ $similar["title"] }}</a></h3>
                                                            <p class="post-meta">{{ $similar["created_at"] }}</p>

                                                        </div>
                                                    </div>

                                                    <!--=======  End of single related post  =======-->
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <!--=======  End of blog related post wrapper  =======-->
                                </div>
                            </div>
                        </div>

                        <!--=======  End of blog related post  =======-->

                    </div>
                </div>
            </div>
        </div>

        <!--=======  End of blog page area  =======-->

    </div>
@endsection

@push("js")

@endpush
