@extends("layouts.frontend._layout")

@section("title")
{{ $title  }}
@endsection

@push("css")

@endpush

@section("content")
    <!--====================  breadcrumb area ====================-->

    <div class="breadcrumb-area section-space--breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <!--=======  breadcrumb wrapper  =======-->

                    <div class="breadcrumb-wrapper">
                        <h2 class="page-title"> {{ $title  }} </h2>
                        <ul class="breadcrumb-list">
                            <li><a href="{{ route("front.index") }}">Anasayfa</a></li>
                            <li class="active">{{ $title }}</li>
                        </ul>
                    </div>

                    <!--=======  End of breadcrumb wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of breadcrumb area  ====================-->

    <!--====================  page content wrapper ====================-->

    <div class="page-content-wrapper">
        <!--=======  shop page area  =======-->

        <div class="shop-page-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 order-2 order-lg-1">
                        <!--=======  shop sidebar wrapper  =======-->

                        <div class="shop-sidebar-wrapper">
                            <input type="hidden" name="maxProductPrice" id="maxProductPrice" value="{{ $maxPrice }}">
                            <input type="hidden" name="minProductPrice" id="minProductPrice" value="{{ $minPrice }}">

                            <!--=======  single sidebar widget  =======-->

                            <div class="single-sidebar-widget">
                                <h2 class="single-sidebar-widget__title">Fiyata Göre Filtrele</h2>
                                <div class="sidebar-price">
                                    <div id="price-range"></div>
                                    <div class="output-wrapper">
                                        <input type="text" id="price-amount" class="price-amount" readonly>
                                        <input type="hidden" name="minPrice" id="minPrice" value="">
                                        <input type="hidden" name="maxPrice" id="maxPrice" value="">
                                    </div>
                                </div>
                            </div>

                            <!--=======  End of single sidebar widget  =======-->

                            <!--=======  single sidebar widget  =======-->

                            <div class="single-sidebar-widget">
                                <h2 class="single-sidebar-widget__title">Ürün Kategorileri</h2>

                                <ul class="single-sidebar-widget__dropdown" id="single-sidebar-widget__dropdown">
                                    @foreach($categories as $category)

                                        <li @if($category["childCount"] > 0 ) class="has-children" @endif ><a href="{{ $category["route"] }}">{{ $category["title"]}}</a>
                                            @if($category["childCount"] > 0 )
                                                <ul class="sub-menu">
                                                    @foreach($category["children"] as $child)
                                                        <li><a href="{{ $child["route"] }}">{{ $child["title"] }}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach

                                </ul>
                            </div>

                            <!--=======  End of single sidebar widget  =======-->

                            <!--=======  single sidebar widget  =======-->
<!--
                            <div class="single-sidebar-widget">
                                <h2 class="single-sidebar-widget__title">Filter By Brand</h2>
                                <ul class="single-sidebar-widget__dropdown">
                                    <li><a href="shop-left-sidebar.html">Alexa</a></li>
                                    <li><a href="shop-left-sidebar.html">Benington</a></li>
                                    <li><a href="shop-left-sidebar.html">Candice</a></li>
                                    <li><a href="shop-left-sidebar.html">Juliet Rowley</a></li>
                                    <li><a href="shop-left-sidebar.html">Olivia Shayn</a></li>
                                    <li><a href="shop-left-sidebar.html">Sarah Stencil</a></li>
                                </ul>
                            </div>
-->
                            <!--=======  End of single sidebar widget  =======-->
                        </div>

                        <!--=======  End of shop sidebar wrapper  =======-->
                    </div>

                    <div class="col-lg-9 order-1 order-lg-2">
                        <!--=======  shop content wrapper  =======-->

                        <div class="shop-content-wrapper">

                            <!--=======  shop header wrapper  =======-->

                            <div class="shop-header">
                                <div class="row align-items-center">
                                    <div class="col-sm-6 col-12">
                                        <!--=======  header left content  =======-->

                                        <div class="shop-header__left">
                                            <p class="result-text d-inline-block mb-0">{{ count($products) }} sonuç getirildi</p>
                                            <div class="view-mode-icons d-inline-block">
                                                <a href="javascript:void(0)" class="active" data-tippy="Izgara Görünümü" data-target="grid" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder"><i class="fa fa-th"></i></a>
                                                <a href="javascript:void(0)" data-target="list" data-tippy="Liste Görünümü" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder"><i class="fa fa-list"></i></a>
                                            </div>
                                        </div>

                                        <!--=======  End of header left content  =======-->
                                    </div>

                                    <div class="col-sm-6 col-12">

                                        <!--=======  header right content  =======-->

                                        <div class="shop-header__right d-flex justify-content-start justify-content-sm-end align-items-center">
                                            <div class="grid-view-changer" id="grid-view-changer">
                                                <a href="javascript:void(0)" id="grid-view-change-trigger" data-tippy="Görüntüleme Seçenekleri" data-target="grid" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <div class="grid-view-changer__menu" id="grid-view-changer__menu">
                                                    <a href="javascript:void(0)" data-target="two-column">2</a>
                                                    <a href="javascript:void(0)" data-target="three-column">3</a>
                                                    <a href="javascript:void(0)" data-target="four-column">4</a>
                                                    <a href="javascript:void(0)" id="grid-view-close-trigger"><i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                            <div class="sort-by-dropdown">
                                                <select name="sort-by" id="sort-by" class="nice-select">
                                                    <option value="0">Varsayılan Sıralama</option>
                                                    <option value="0">Fiyata Göre (Artan)</option>
                                                    <option value="0">Fiyata Göre(Azalan)</option>
                                                    <option value="0">Eklenme Tarihi (Artan)</option>
                                                    <option value="0">Eklenme Tarihi (Azalan)</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!--=======  End of header right content  =======-->

                                    </div>
                                </div>
                            </div>

                            <!--=======  End of shop header wrapper  =======-->

                            <!--=======  shop product wrapper  =======-->

                            <div class="shop-product-wrap shop-product-wrap--with-sidebar row grid">

                                @foreach($products as $product)
                                    <div class="col-lg-4 col-md-6 col-sm-6 col-custom-sm-6 col-12">

                                    <!--=======  grid view product  =======-->

                                    <div class="single-grid-product">
                                        <div class="single-grid-product__image">
                                            <div class="product-badge-wrapper">
                                                <span class="hot">Fırsat Ürünü</span>
                                            </div>
                                            <a href="{{ $product["route"] }}" class="image-wrap">
                                                <img src="{{ $product["coverImage"] }}" class="img-fluid" alt="{{ $product["title"] }}">
                                                <img src="{{ $product["secondCover"] }}" class="img-fluid" alt="{{ $product["title"] }}">
                                            </a>
                                            <div class="product-hover-icon-wrapper">
                                               <span class="single-icon single-icon--add-to-cart"><a href="#" data-tippy="Sepete Ekle" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme = "sharpborder" > <i class="fa fa-shopping-basket"></i> <span>Sepete Ekle</span> </a></span>
                                               </div>
                                        </div>
                                        <div class="single-grid-product__content">
                                            <h3 class="title"><a href="{{ $product["route"] }}">Lighting Lamp</a></h3>
                                            <div class="price"><span class="main-price {{ $product["discountPrice"] != 0 ? "discounted": "" }}">{{ $product["price"] }}</span>
                                                @if($product["discountPrice"] != 0)
                                                    <span class="discounted-price">{{ $product["discountPrice"] }}</span>
                                                @endif
                                            </div>
                                            <div class="rating">
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star active"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <a href="#" class="favorite-icon" data-tippy="Add to Wishlist" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder" data-tippy-placement="left">
                                                <i class="fa fa-heart-o"></i>
                                                <i class="fa fa-heart"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <!--=======  End of grid view product  =======-->

                                    <!--=======  list view product  =======-->

                                    <div class="single-list-product">

                                        <div class="single-list-product__image">
                                            <a href="#" class="favorite-icon" data-tippy="Add to Wishlist" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder" data-tippy-placement="left">
                                                <i class="fa fa-heart-o"></i>
                                                <i class="fa fa-heart"></i>
                                            </a>

                                            <div class="product-badge-wrapper">
                                                <span class="onsale">-17%</span>
                                                <span class="hot">Hot</span>
                                            </div>
                                            <a href="{{ $product["route"] }}" class="image-wrap">
                                                <img src="{{ $product["coverImage"] }}" class="img-fluid" alt="{{ $product["title"] }}">
                                                <img src="{{ $product["secondCover"] }}" class="img-fluid" alt="{{ $product["title"] }}">
                                            </a>



                                        </div>

                                        <div class="single-list-product__content">
                                            <h3 class="title"><a href="{{ $product["route"] }}">{{ $product["title"] }}</a></h3>
                                            <div class="price"><span class="main-price {{ $product["discountPrice"] != 0 ? "discounted": "" }}">{{ $product["price"] }}</span>
                                                @if($product["discountPrice"] != 0)
                                                    <span class="discounted-price">{{ $product["discountPrice"] }}</span>
                                                @endif
                                            </div>
                                            <p class="product-short-desc">{{ $product["shortDescription"] }}</p>

                                            <div class="product-hover-icon-wrapper">
                                                <span class="single-icon single-icon--add-to-cart"><a href="#" data-tippy="Sepete Ekle" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme = "sharpborder" ><i class="fa fa-shopping-basket"></i> <span>Sepete Ekle</span></a></span>
                                            </div>

                                        </div>
                                    </div>

                                    <!--=======  End of list view product  =======-->

                                </div>
                                @endforeach



                            </div>

                            <!--=======  End of shop product wrapper  =======-->

                            <!--=======  pagination wrapper  =======-->
<!--
                            <div class="pagination-wrapper">
                                <ul>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
-->
                            <!--=======  End of pagination wrapper  =======-->

                        </div>

                        <!--=======  End of shop content wrapper  =======-->
                    </div>
                </div>
            </div>
        </div>

        <!--=======  End of shop page area  =======-->
    </div>

    <!--====================  End of page content wrapper  ====================-->


@endsection

@push("js")

@endpush
