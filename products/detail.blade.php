@extends("layouts.frontend._layout")

@section("title")
    {{ $title  }}
@endsection

@push("css")

@endpush

@section("content")
    <!--====================  breadcrumb area ====================-->

    <div class="breadcrumb-area section-space--breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <!--=======  breadcrumb wrapper  =======-->

                    <div class="breadcrumb-wrapper">
                        <h2 class="page-title">{{ $title }}</h2>
                        <ul class="breadcrumb-list">
                            <li><a href="{{ route("front.index") }}">Anasayfa</a></li>
                            <li><a href="{{ route("front.product-list") }}">Ürünler</a></li>
                            <li class="active">{{ $title }}</li>
                        </ul>
                    </div>

                    <!--=======  End of breadcrumb wrapper  =======-->
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of breadcrumb area  ====================-->

    <!--====================  page content wrapper ====================-->

    <div class="page-content-wrapper">
        <!--=======  single product slider details area  =======-->

        <div class="single-product-slider-details-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <!--=======  product details slider area  =======-->

                        <div class="product-details-slider-area product-details-slider-area--side-move">

                            <div class="product-badge-wrapper">
                                <span class="hot">Fırsat Ürünü</span>
                            </div>

                            <div class="row row-5">
                                <div class="col-md-9 order-1 order-md-2">
                                    <div class="big-image-wrapper">
                                        <div class="enlarge-icon">
                                            <a class="btn-zoom-popup" href="javascript:void(0)" data-tippy="Click to enlarge" data-tippy-placement="left" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder"><i class="pe-7s-expand1"></i></a>
                                        </div>
                                        <div class="product-details-big-image-slider-wrapper product-details-big-image-slider-wrapper--side-space theme-slick-slider" data-slick-setting='{
                    "slidesToShow": 1,
                    "slidesToScroll": 1,
                    "arrows": false,
                    "autoplay": false,
                    "autoplaySpeed": 5000,
                    "fade": true,
                    "speed": 500,
                    "prevArrow": {"buttonClass": "slick-prev", "iconClass": "fa fa-angle-left" },
                    "nextArrow": {"buttonClass": "slick-next", "iconClass": "fa fa-angle-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1501, "settings": {"slidesToShow": 1, "arrows": false} },
                    {"breakpoint":1199, "settings": {"slidesToShow": 1, "arrows": false} },
                    {"breakpoint":991, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                    {"breakpoint":767, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                    {"breakpoint":575, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} },
                    {"breakpoint":479, "settings": {"slidesToShow": 1, "arrows": false, "slidesToScroll": 1} }
                ]'>
                                            @foreach($product["images"] as $image)
                                            <div class="single-image">
                                                <img src="{{ getImage("products", $image->image) }}" class="img-fluid" alt="{{ $product["title"] }}">
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 order-2 order-md-1">
                                    <div class="product-details-small-image-slider-wrapper product-details-small-image-slider-wrapper--vertical-space theme-slick-slider" data-slick-setting='{
                "slidesToShow": 3,
                "slidesToScroll": 1,
                "centerMode": false,
                "arrows": true,
                "vertical": true,
                "autoplay": false,
                "autoplaySpeed": 5000,
                "speed": 500,
                "asNavFor": ".product-details-big-image-slider-wrapper",
                "focusOnSelect": true,
                "prevArrow": {"buttonClass": "slick-prev", "iconClass": "fa fa-angle-up" },
                "nextArrow": {"buttonClass": "slick-next", "iconClass": "fa fa-angle-down" }
            }' data-slick-responsive='[
                {"breakpoint":1501, "settings": {"slidesToShow": 3, "arrows": true} },
                {"breakpoint":1199, "settings": {"slidesToShow": 3, "arrows": true} },
                {"breakpoint":991, "settings": {"slidesToShow": 3, "arrows": true, "slidesToScroll": 1} },
                {"breakpoint":767, "settings": {"slidesToShow": 3, "arrows": false, "slidesToScroll": 1, "vertical": false, "centerMode": true} },
                {"breakpoint":575, "settings": {"slidesToShow": 3, "arrows": false, "slidesToScroll": 1, "vertical": false, "centerMode": true} },
                {"breakpoint":479, "settings": {"slidesToShow": 2, "arrows": false, "slidesToScroll": 1, "vertical": false, "centerMode": true} }
            ]'>
                                        @foreach($product["images"] as $image)
                                            <div class="single-image">
                                                <img src="{{ getImage("products", $image->image) }}" class="img-fluid" alt="{{ $product["title"] }}">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>



                        </div>

                        <!--=======  End of product details slider area  =======-->
                    </div>
                    <div class="col-lg-6">
                        <!--=======  product details description area  =======-->

                        <div class="product-details-description-wrapper">
                            <h2 class="item-title">{{ $product["title"] }}</h2>
                            <div class="price"><span class="main-price {{ $product["discountPrice"] != 0 ? "discounted": "" }}">{{ $product["price"] }}</span>
                                @if($product["discountPrice"] != 0)
                                    <span class="discounted-price">{{ $product["discountPrice"] }}</span>
                                @endif
                            </div>

                            <p class="description">{!! $product["shortDescription"]  !!}</p>


                            <div class="pro-qty d-inline-block">
                                <input type="text" value="1">
                            </div>

                            <div class="add-to-cart-btn d-inline-block">
                                <button class="theme-button theme-button--alt">Sepete Ekle</button>
                            </div>

                            <div class="quick-view-other-info">
                                <div class="other-info-links">
                                    <a href="javascript:void(0)"><i class="fa fa-heart-o"></i> ADD TO WISHLIST</a>

                                </div>

                                <div class="product-brand">
                                    <a href="shop-left-sidebar.html">
                                        <img src="assets/img/brands/brand-2.png" class="img-fluid" alt="">
                                    </a>
                                </div>

                                <table>
                                    <tr class="single-info">
                                        <td class="quickview-title">Kategori: </td>
                                        <td class="quickview-value">
                                            <a href="{{ $product["categoryRoute"] }}">{{ $product["categoryTitle"] }}</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <!--=======  End of product details description area  =======-->
                    </div>
                </div>
            </div>
        </div>

        <!--=======  End of single product slider details area  =======-->

        <!--=======  single product description tab area  =======-->

        <div class="single-product-description-tab-area section-space">
            <!--=======  description tab navigation  =======-->

            <div class="description-tab-navigation">
                <div class="nav nav-tabs justify-content-center" id="nav-tab2" role="tablist">
                    <a class="nav-item nav-link active" id="description-tab" data-toggle="tab" href="#product-description" role="tab" aria-selected="true">Açıklama</a>
                    <a class="nav-item nav-link" id="additional-info-tab" data-toggle="tab" href="#product-additional-info" role="tab" aria-selected="false">Ek Açıklama</a>
                </div>
            </div>

            <!--=======  End of description tab navigation  =======-->

            <!--=======  description tab content  =======-->


            <div class="single-product-description-tab-content">

                <div class="tab-content">

                    <div class="tab-pane fade show active" id="product-description" role="tabpanel" aria-labelledby="description-tab">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--=======  description content  =======-->

                                    <div class="description-content">
                                        <p class="long-desc">{{ $product["description"] }}</p>

                                    </div>

                                    <!--=======  End of description content  =======-->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="product-additional-info" role="tabpanel" aria-labelledby="additional-info-tab">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--=======  additional info content  =======-->

                                    <div class="additional-info-content">
                                        <table class="additional-info-table">
                                            <tbody>
                                            <tr>
                                                <th>Kategori</th>
                                                <td class="product_dimensions">{{ $product["categoryTitle"] }}</td>
                                            </tr>

                                            <tr>
                                                <th>Orijinal Fiyat</th>
                                                <td>
                                                    <p>{{ $product["price"] }}</p>
                                                </td>
                                            </tr>

                                            @if($product["discountPrice"] != 0 || $product["discountPrice"] != null)
                                                <tr>
                                                    <th>İndirimli Fiyat</th>
                                                    <td>
                                                        <p>{{ $product["discountPrice"] }}</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>İndirim Yüzdesi (%)</th>
                                                    <td>
                                                        <p>{{ calcPercantage($product["price"], $product["discountPrice"]) }} %</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>

                                    <!--=======  End of additional info content  =======-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <!--=======  End of description tab content  =======-->

        </div>

        <!--=======  End of single product description tab area  =======-->

        <!--====================  related product slider area ====================-->

        <div class="product-slider-area section-space">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="section-title-area text-center">
                            <h2 class="section-title">İlginizi Çekebilir</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <!--=======  product slider wrapper  =======-->

                        <div class="product-slider-wrapper theme-slick-slider" data-slick-setting='{
                        "slidesToShow": 4,
                        "slidesToScroll": 4,
                        "arrows": true,
                        "dots": true,
                        "autoplay": false,
                        "speed": 500,
                        "prevArrow": {"buttonClass": "slick-prev", "iconClass": "fa fa-angle-left" },
                        "nextArrow": {"buttonClass": "slick-next", "iconClass": "fa fa-angle-right" }
                    }' data-slick-responsive='[
                        {"breakpoint":1501, "settings": {"slidesToShow": 4, "slidesToScroll": 4, "arrows": false} },
                        {"breakpoint":1199, "settings": {"slidesToShow": 3, "slidesToScroll": 3, "arrows": false} },
                        {"breakpoint":991, "settings": {"slidesToShow": 2,"slidesToScroll": 2, "arrows": true, "dots": false} },
                        {"breakpoint":767, "settings": {"slidesToShow": 2,"slidesToScroll": 2,  "arrows": true, "dots": false} },
                        {"breakpoint":575, "settings": {"slidesToShow": 2, "slidesToScroll": 2,"arrows": false, "dots": true} },
                        {"breakpoint":479, "settings": {"slidesToShow": 1,"slidesToScroll": 1, "arrows": true, "dots": false} }
                    ]'>

                            <div class="col">
                                <!--=======  single short view product  =======-->

                                @foreach($similars as $similar)
                                <div class="single-grid-product">
                                    <div class="single-grid-product__image">
                                        <div class="product-badge-wrapper">
                                            <span class="hot">Keşfet</span>
                                        </div>
                                        <a href="{{ $similar["route"] }}" class="image-wrap">
                                            <img src="{{ $similar["coverImage"] }}" class="img-fluid" alt="{{ $similar["title"] }}">
                                            <img src="{{ $similar["secondCover"] }}" class="img-fluid" alt="{{ $similar["title"] }}">
                                        </a>
                                        <div class="product-hover-icon-wrapper">
                                            <span class="single-icon single-icon--add-to-cart"><a href="#" data-tippy="Sepete Ekle" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme = "sharpborder" ><i class="fa fa-shopping-basket"></i> <span>Sepete Ekle</span></a></span>
                                          </div>
                                    </div>
                                    <div class="single-grid-product__content">
                                        <h3 class="title"><a href="{{ $similar["route"] }}">{{ $similar["title"] }}</a></h3>
                                        <div class="price"><span class="main-price {{ $similar["discountPrice"] != 0 ? "discounted": "" }}">{{ $similar["price"] }}</span>
                                            @if($similar["discountPrice"] != 0)
                                                <span class="discounted-price">{{ $similar["discountPrice"] }}</span>
                                            @endif
                                        </div>
                                        <div class="rating">
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star active"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <a href="#" class="favorite-icon" data-tippy="Add to Wishlist" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder" data-tippy-placement="left">
                                            <i class="fa fa-heart-o"></i>
                                            <i class="fa fa-heart"></i>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                                <!--=======  End of single short view product  =======-->
                            </div>



                        </div>

                        <!--=======  End of product slider wrapper  =======-->
                    </div>
                </div>
            </div>
        </div>

        <!--====================  End of related product slider area  ====================-->

    </div>

    <!--====================  End of page content wrapper  ====================-->

@endsection

@push("js")

@endpush
