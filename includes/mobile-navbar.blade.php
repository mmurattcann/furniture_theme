<!--=======  mobile navigation area  =======-->

<div class="header-mobile-navigation d-block d-lg-none">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-6 col-md-6">
                <div class="header-logo">
                    <a href="index.html">
                        <img src="{{frontAsset("img/logo.png")}}" class="img-fluid" alt="">
                    </a>
                </div>
            </div>
            <div class="col-6 col-md-6">
                <div class="mobile-navigation text-right">
                    <ul class="header-icon__list header-icon__list">
                        <li>
                            <a href="wishlist.html"><i class="fa fa-heart-o"></i><span class="item-count">1</span></a>
                        </li>
                        <li>
                            <a href="cart.html"><i class="fa fa-shopping-basket"></i><span class="item-count">3</span></a>
                        </li>
                        <li><a href="javascript:void(0)" class="mobile-menu-icon" id="mobile-menu-trigger"><i class="fa fa-bars"></i></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>

<!--=======  End of mobile navigation area  =======-->
