<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ isset($title) ?  $title. " | " : "" }} {{ env("APP_NAME") }} </title>
    <meta name="description" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="{{ frontAsset("img/favicon.ico") }}">

    <!--=============================================
    =            CSS  files       =
    =============================================-->

    <!-- Vendor CSS -->
    <link href="{{ frontAsset("css/vendors.css") }}" rel="stylesheet">
    <!-- Main CSS -->
    <link href="{{ frontAsset("css/style.css") }}" rel="stylesheet">


    <!-- Revolution Slider CSS -->
    <link href="{{ frontAsset("revolution/css/settings.css") }}" rel="stylesheet">
    <link href="{{ frontAsset("revolution/css/navigation.css") }}" rel="stylesheet">
    <link href="{{ frontAsset("revolution/custom-setting.css") }}" rel="stylesheet">


    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

</head>

<body>

<!--====================  header area ====================-->

<div class="header-area header-area--default header-area--default--white header-sticky">

    <!--=======  header navigation wrapper  =======-->

@include("front.furniture_theme.includes.navbar")

    <!--=======  End of header navigation wrapper  =======-->

    <!--=======  mobile navigation area  =======-->
    @include("front.furniture_theme.includes.mobile-navbar")

    <!--=======  End of mobile navigation area  =======-->
</div>
