<!--====================  footer ====================-->

<div class="footer-area">
    <div class="footer-navigation-area">
        <div class="container wide">
            <div class="row">
                <div class="col-xl-12 col-custom-xl-12 col-lg-12">
                    <div class="row">
                        @foreach($footers as $footer)
                            <div class="col-4 col-sm-4">
                                <div class="footer-widget">
                                    <h4 class="footer-widget__title">{{ $footer["title"] }}</h4>
                                    <nav class="footer-widget__navigation">

                                        <ul>
                                            @foreach($footer["children"] as $child)
                                                <li><a href="{{$child["route"]}}">{{$child["title"]}}</a></li>
                                            @endforeach
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-copyright-area">
        <div class="container wide">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright-text text-center">
                        copyright &copy; 2019 <a href="#">Robin</a>. All Rights Reserved
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--====================  End of footer  ====================-->
<!--====================  offcanvas items ====================-->

<!--=======  offcanvas mobile menu  =======-->

<div class="offcanvas-mobile-menu" id="offcanvas-mobile-menu">
    <a href="javascript:void(0)" class="offcanvas-menu-close" id="offcanvas-menu-close-trigger">
        <i class="pe-7s-close"></i>
    </a>

    <div class="offcanvas-wrapper">

        <div class="offcanvas-inner-content">
            <div class="offcanvas-mobile-search-area">
                <form action="#">
                    <input type="search" placeholder="Ara ...">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <nav class="offcanvas-naviagtion">
                <ul>
                    @foreach($menus as $menu)
                        <li class="menu-item{{$menu["childCount"] > 0 ? "-has-children" : "" }}"><a href="{{$menu["route"]}}">{{ $menu["title"] }}</a>

                            @if($menu["childCount"] > 0)
                                <ul class="sub-menu">
                                    @foreach($menu["children"] as $child)
                                        <li class="menu-item{{$child["childCount"] > 0 ? "-has-children" : "" }}"><a href="{{ $child["route"] }}">{{ $child["title"] }}</a>
                                            @if($child["childCount"] > 0)
                                                <ul class="sub-menu">
                                                    @foreach($child["children"] as $grandChild)
                                                        <li><a href="{{ $grandChild["route"] }}">{{ $grandChild["title"] }}</a> </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach

                </ul>
            </nav>

            <div class="offcanvas-widget-area">
                <div class="off-canvas-contact-widget">
                    <div class="header-contact-info">
                        <ul class="header-contact-info__list">
                            <li><i class="pe-7s-phone"></i> <a href="tel://12452456012">(1245) 2456 012 </a></li>
                            <li><i class="pe-7s-mail-open"></i> <a href="mailto:info@yourdomain.com">info@yourdomain.com</a></li>
                        </ul>
                    </div>
                </div>
                <!--Off Canvas Widget Social Start-->
                <div class="off-canvas-widget-social">
                    <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                    <a href="#" title="Youtube"><i class="fa fa-youtube-play"></i></a>
                    <a href="#" title="Vimeo"><i class="fa fa-vimeo-square"></i></a>
                </div>
                <!--Off Canvas Widget Social End-->
            </div>
        </div>
    </div>

</div>

<!--=======  End of offcanvas mobile menu  =======-->

<!--====================  End of offcanvas items  ====================-->
<!--=======  search overlay  =======-->

<div class="search-overlay" id="search-overlay">

    <!--=======  close icon  =======-->

    <span class="close-icon search-close-icon">
        <a href="javascript:void(0)"  id="search-close-icon">
            <i class="pe-7s-close"></i>
        </a>
    </span>

    <!--=======  End of close icon  =======-->

    <!--=======  search overlay content  =======-->

    <div class="search-overlay-content">
        <div class="input-box">
            <form action="index.html">
                <input type="search" placeholder="Ürünleri Ara...">
            </form>
        </div>
        <div class="search-hint">
            <span># Hit enter to search or ESC to close</span>
        </div>
    </div>

    <!--=======  End of search overlay content  =======-->
</div>

<!--=======  End of search overlay  =======-->
<!--=============================================
=            quick view         =
=============================================-->

<div id="qv-1" class="cd-quick-view">
    <div class="cd-slider-wrapper">
        <div class="product-badge-wrapper">
            <span class="onsale">-17%</span>
            <span class="hot">Hot</span>
        </div>
        <ul class="cd-slider">
            <li class="selected"><img src="assets/img/products/product-9-1-600x800.jpg" alt="Product 2"></li>
            <li><img src="assets/img/products/product-9-2-600x800.jpg" alt="Product 1"></li>
        </ul> <!-- cd-slider -->

        <ul class="cd-slider-pagination">
            <li class="active"><a href="#0">1</a></li>
            <li><a href="#1">2</a></li>
        </ul> <!-- cd-slider-pagination -->

        <ul class="cd-slider-navigation">
            <li><a class="cd-prev" href="#0"><i class="fa fa-angle-left"></i></a></li>
            <li><a class="cd-next" href="#0"><i class="fa fa-angle-right"></i></a></li>
        </ul> <!-- cd-slider-navigation -->
    </div> <!-- cd-slider-wrapper -->

    <div class="quickview-item-info cd-item-info ps-scroll">

        <h2 class="item-title">Atelier Ottoman Takumi Series</h2>
        <p class="price">
            <span class="main-price discounted">$360.00</span>
            <span class="discounted-price">$300.00</span>
        </p>

        <p class="description">Upholstered velvet ottoman with antique stud detailing. Invite restrained glamour and on-trend colour into your design scheme with the Eichholtz Ponti Ottoman. Inspired by the neo-classical influences of Italian icon, Gio Ponti, this contemporary ottoman collection is presented in a choice of velvet and linen colourways.</p>


        <div class="pro-qty d-inline-block">
            <input type="text" value="1">
        </div>

        <div class="add-to-cart-btn d-inline-block">
            <button class="theme-button theme-button--alt">ADD TO CART</button>
        </div>

        <div class="quick-view-other-info">
            <div class="other-info-links">
                <a href="javascript:void(0)"><i class="fa fa-heart-o"></i> ADD TO WISHLIST</a>
                <a href="javascript:void(0)"><i class="fa fa-exchange"></i> COMPARE</a>
            </div>
            <table>
                <tr class="single-info">
                    <td class="quickview-title">SKU: </td>
                    <td class="quickview-value">12345</td>
                </tr>
                <tr class="single-info">
                    <td class="quickview-title">Categories: </td>
                    <td class="quickview-value">
                        <a href="#">Decor</a>,
                        <a href="#">Living Room</a>,
                        <a href="#">Furniture</a>
                    </td>
                </tr>
                <tr class="single-info">
                    <td class="quickview-title">Tags: </td>
                    <td class="quickview-value">
                        <a href="#">Decor</a>,
                        <a href="#">Light</a>
                    </td>
                </tr>
                <tr class="single-info">
                    <td class="quickview-title">Share on: </td>
                    <td class="quickview-value">
                        <ul class="quickview-social-icons">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>


    </div> <!-- cd-item-info -->
    <a href="#0" class="cd-close">Close</a>
</div>

<!--=====  End of quick view  ======-->
<!-- scroll to top  -->
<button class="scroll-top">
    <i class="fa fa-angle-up"></i>
</button>
<!-- end of scroll to top -->
<!--=============================================
=            JS files        =
=============================================-->

<!-- Vendor JS -->
<script src="{{frontAsset("js/vendors.js")}}"></script>

<!-- Active JS -->
<script src="{{frontAsset("js/active.js")}}"></script>

<!--=====  End of JS files ======-->


<!-- Revolution Slider JS -->
<script src="{{frontAsset("revolution/js/jquery.themepunch.revolution.min.js")}}"></script>
<script src="{{frontAsset("revolution/js/jquery.themepunch.tools.min.js")}}"></script>
<script src="{{frontAsset("revolution/revolution-active.js")}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{frontAsset("revolution/js/extensions/revolution.extension.kenburn.min.js")}}"></script>
<script type="text/javascript" src="{{frontAsset("revolution/js/extensions/revolution.extension.slideanims.min.js")}}"></script>
<script type="text/javascript" src="{{frontAsset("revolution/js/extensions/revolution.extension.actions.min.js")}}"></script>
<script type="text/javascript" src="{{frontAsset("revolution/js/extensions/revolution.extension.layeranimation.min.js")}}"></script>
<script type="text/javascript" src="{{frontAsset("revolution/js/extensions/revolution.extension.navigation.min.js")}}"></script>
<script type="text/javascript" src="{{frontAsset("revolution/js/extensions/revolution.extension.parallax.min.js")}}"></script>

<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>

<script src="{{frontAsset("js/blockUI.js")}}"></script>
<script src="{{frontAsset("js/custom.js")}}"></script>
</body>

</html>
